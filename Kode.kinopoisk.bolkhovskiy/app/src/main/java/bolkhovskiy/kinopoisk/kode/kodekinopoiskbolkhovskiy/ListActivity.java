package bolkhovskiy.kinopoisk.kode.kodekinopoiskbolkhovskiy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ListActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "ListFilms";
    private ProgressDialog pDialog;
    private ListView lv;
    Context context;
    public String jsonStr, filmID, message;
    public String[] FilmInfos;
    ArrayList<HashMap<String, String>> FilmList;
    private String TAG = ListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Intent intent = getIntent();
        jsonStr = intent.getStringExtra(EXTRA_MESSAGE);
        FilmList = new ArrayList<>();
        lv = (ListView) findViewById(R.id.list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListActivity.this, InfoActivity.class);
                intent.putExtra(InfoActivity.EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });
        new GetFilms().execute();
    }

    /**
     * Async task class to get films
     */
    private class GetFilms extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ListActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
           // String str = jsonStr;
            JSONObject jsonObj=null;
            if (jsonStr != null) {
                try {

                    try {
                       jsonObj = new JSONObject(jsonStr);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    // Getting JSON Array node
                    JSONArray films = jsonObj.getJSONArray("filmsData");

                    for (int i = 0; i < films.length(); i++) {
                        JSONObject film = films.getJSONObject(i);

                        String nameRU = film.getString("nameRU");
                        String nameEN = film.getString("nameEN");
                        String rating = film.getString("rating");
                        String genre = film.getString("genre");
                        filmID = film.getString("id");

                        HashMap<String, String> contact = new HashMap<>();
                        contact.put("nameRU", nameRU);
                        contact.put("nameEN", nameEN);
                        contact.put("rating", rating);
                        contact.put("genre", genre);
                        contact.put("id", filmID); //<----------------------------111111111

                        // adding contact to contact list
                        FilmList.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            ListAdapter adapter = new SimpleAdapter(
                    ListActivity.this, FilmList,
                    R.layout.list_item, new String[]{"nameRU", "nameEN",
                    "rating", "genre", "id"}, new int[]{R.id.nameRU,
                    R.id.nameEN, R.id.rating, R.id.genre, R.id.filmID});

            lv.setAdapter(adapter);
            new ParseInfo().execute();
        }

    }

   // public void getFilmInfo(View view){
   //     filmID = "";
   //     TextView textv = (TextView) findViewById(R.id.filmID);
   //     filmID = textv.getText().toString();
   //     new ParseInfo().execute();
   // }

    private class ParseInfo extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            // получаем данные с внешнего ресурса
            try {
                URL url = new URL("http://api.kinopoisk.cf/getFilm?filmID="+filmID);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            message = DecodeUnicode.decodeUnicode(strJson);

        }
    }


}
