package bolkhovskiy.kinopoisk.kode.kodekinopoiskbolkhovskiy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InfoActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "FilmInfo";
    private ProgressDialog pDialog;
    public String filmInfo;
    private ListView lv;
    public String jsonStr, filmID, message;
    ArrayList<HashMap<String, String>> FilmInfo;
    private String TAG = ListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Intent intent = getIntent();
        filmInfo = intent.getStringExtra(EXTRA_MESSAGE);
        FilmInfo = new ArrayList<>();
        lv = (ListView) findViewById(R.id.list);
        new GetFilms().execute();
    }

    /**
     * Async task class to get films
     */
    private class GetFilms extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(InfoActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // String str = jsonStr;
            JSONObject jsonObjct=null;
            if (filmInfo != null) {
                try {
                 jsonObjct = new JSONObject(filmInfo);
                        String nameRU = jsonObjct.getString("nameRU");
                        String genre = jsonObjct.getString("genre");
                        String Info = jsonObjct.getString("description");
                        HashMap<String, String> contact = new HashMap<>();
                        contact.put("nameRU", nameRU);
                        contact.put("genre", genre);
                        contact.put("description", Info);

                        FilmInfo.add(contact);
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            ListAdapter adapter = new SimpleAdapter(
                    InfoActivity.this, FilmInfo,
                    R.layout.info_item, new String[]{"nameRU", "genre",
                    "description"}, new int[]{R.id.nameRU,
                    R.id.genre, R.id.filmInfo});

            lv.setAdapter(adapter);
        }

    }
}
